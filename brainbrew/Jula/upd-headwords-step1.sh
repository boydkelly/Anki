#!/usr/bin/bash
#PURPOSE:  update the main.tsv file with the records from hw-anki-words.tsv (dictionary)
# fields note1 and note4 , example 1 to 4, text 2 to 4 not overwritten
# this should also add any new items from headword-5.tsv to main.tsv
# after running this script, make edits to import.tsv.
# then move to upate.tsv and run the next script to update main.tsv
# then run brainbrew to update crowdanki file
# then import to anki
set -o errexit
#set -o nounset
set -o pipefail
source ./functions.sh
for x in csvtool csvclean gawk dos2unix; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 1
  }
done

[ -z "$1" ] && {
  echo "Specify french or jula"
  exit 1
}

case $1 in
"jula")
  SORT="-k2"
  ;;
"french")
  SORT="-k5"
  ;;
esac

UPDATE=./src/data/update.tsv
HEADWORDS="/home/bkelly/dev/jula/dyu-xdxf/build/lexicon.tsv"
MAIN="./src/data/Main.tsv"

[ -f ${HEADWORDS} ] || {
  echo "${HEADWORDS} not found.  Exiting..."
  exit 1
}
validate $HEADWORDS
validate $MAIN

#what the heck is this read -r REPLY for?
awk -f hw5.awk ${HEADWORDS} ${MAIN} | (
  read -r
  printf "%s\n" "$REPLY"
  sort -t $'\t' $SORT
) >$UPDATE

#csvlint -delimiter '\t' $OUT
./report.sh
