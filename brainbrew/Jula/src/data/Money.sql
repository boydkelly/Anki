select 
 substr(
        a.flds
       ,0
       ,b.First
       ) as Note_ID             

,substr(
        a.flds
       ,b.First
       ,b.Second - b.First
       ) as Source   

,substr(
        a.flds
       ,b.Second
       ,b.Third - b.Second
       ) as French   

,substr(
        a.flds
       ,b.Third
       ,b.Fourth
       ) as Jula1        
        
,substr(
        a.flds
       ,b.Fourth + b.Third
       ,b.Fifth
       ) as Note1   


,substr(
        a.flds
       ,b.Fifth + b.Fourth + b.Third
       ,LENGTH(a.flds)
       ) as Picture       
,a.tags
from notes a
left join (
          select id
                ,First
                ,Second
                ,Third                
                ,INSTR(
                      substr(
                             flds  
                            ,Third + 1     
                            ,LENGTH(flds) 
                            )
                     ,''
                     ) as Fourth 
                   
                ,INSTR(
                      substr(
                             flds  
                            ,INSTR(
                                   substr(
                                          flds  
                                         ,Third + 1   
                                         ,LENGTH(flds) 
                                         )
                                   ,''
                             )  + Third + 1
                            ,LENGTH(flds) 
                            )
                     ,''
                     ) as Fifth 

          from (
                select a1.id               
                      ,a1.flds
                      ,INSTR(a1.flds,'') as First
					  
                      ,INSTR(
                             substr(
                                    a1.flds
                                   ,INSTR(a1.flds,'') + 1
                                   ,LENGTH(a1.flds) 
                                   ) 
                            ,''
                            ) + INSTR(a1.flds,'') as Second   
							
                      ,INSTR(
                             substr(
                                    a1.flds
                                   ,( INSTR(
                                            substr(
                                                   a1.flds
                                                  ,INSTR(a1.flds,'') + 1
                                                  ,LENGTH(a1.flds) 
                                                  ) 
                                            ,''
                                            ) + INSTR(a1.flds,'')
                                    ) + 1 
                                    ,LENGTH(a1.flds) 
                                    ) 
                            ,''
                            ) + INSTR(
                                      substr(
                                             a1.flds
                                            ,INSTR(a1.flds,'') + 1
                                            ,LENGTH(a1.flds) 
                                            ) 
                                      ,''
                                      ) + INSTR(a1.flds,'') as Third
                from notes a1
                )
          ) as b on a.id = b.id          
/*
where flds like '%55856535157%'
or flds like '%8333450488%'
or flds like '%55849597984%'
or flds like '%54141988125%'
or flds like '%44613202059%'
*/