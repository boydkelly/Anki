#!/usr/bin/bash

for x in dos2unix curl mailx; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

VOCAB="/home/bkelly/Downloads/Dico FrancoJula - Vocab.tsv"
WT="/home/bkelly/Downloads/Dico FrancoJula - WT.tsv"
VOCAB="Vocab.tsv"
WT="WT.tsv"

curl -s "https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=1005751682&single=true&output=tsv" > ${VOCAB} || { echo "Download ${VOCAB} failed"; exit 1; }

curl -s "https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=703917505&single=true&output=tsv" > ${WT}


[ -f "${VOCAB}" ] || { echo "${VOCAB} not found."; exit 1; } 
[ -f "${WT}" ] || { echo "${WT} not found."; exit 1; } 

git checkout master

git pull origin master
git checkout -b spreadsheet
awk -F'\t' '$1' "${WT}" | grep -v "Note ID" | sort | dos2unix > Jula-Master-WT.tsv
awk -F'\t' '$1' "${VOCAB}" | grep -v "Note ID" | sort | dos2unix > Jula-Master-Vocab.tsv
git commit -a -m "updated spreadsheet branch  by script"
git checkout master
git merge spreadsheet

git branch -d spreadsheet
git push origin master

cp *.tsv ~/dev/blog/static/download/
pushd ~/dev/blog
git commit -a -m "anki updates by script"
git push origin master
popd
git status

#git checkout --patch spreadsheet merge/Jula-Vocab.tsv
#git checkout --patch spreadsheet merge/Jula-Vocab.tsv
