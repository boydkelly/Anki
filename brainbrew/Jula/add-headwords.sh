#!/usr/bin/bash
#PURPOSE:  update the main.tsv file with the records from headwords-5.tsv (dictionary)
# fields note1 and note4 , example 1 to 4, text 2 to 4 not overwritten
# this should also add any new items from headword-5.tsv to main.tsv
# after running this script, make edits to update.tsv.
# then  run the next script to update main.tsv
# then run brainbrew to update crowdanki file
# then import to anki
set -o errexit
set -o nounset
set -o pipefail
for x in csvtool csvclean gawk dos2unix; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

MAIN="./src/data/Main.tsv"
HEADWORDS="./src/data/headwords-5.tsv"
UPDATE=./src/data/update.tsv

function validate {
[ -f ${1} ] || { echo "${1} not found.  Exiting..."; exit 1; }
echo Validating $1
  csvclean -n -v -t $1
  awk 'BEGIN{FS="\t"} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
}

validate $HEADWORDS
validate $MAIN

awk -f hw5.awk ${HEADWORDS} ${MAIN} | (read -r; printf "%s\n" "$REPLY"; sort -t $'\t' -k2) > $UPDATE

validate $UPDATE 
./report.sh

git diff-index --quiet HEAD -- || git commit -a -m "updated on: $(date) by $0"
sed -n '/^\s*$/p' $UPDATE | wc -l
sed -i '/^\s*$/d' $UPDATE 
git diff-index --quiet HEAD -- || git commit -a -m "sed del blank lines by $0"
validate $UPDATE

#make sure to pull and changes first
git -C ./build/Jula pull origin main

brainbrew run ./recipes/update.yaml
git -C ./build/Jula diff-index --quiet HEAD -- || git -C ./build/Jula commit -a -m "brainbrew run update by $0"
git -C ./build/Jula push origin main
./report.sh

brainbrew run ./recipes/anki_to_source.yaml
git diff-index --quiet HEAD -- || git commit -a -m "brainbrew run anki to source by $0"
git push origin main
./report.sh

