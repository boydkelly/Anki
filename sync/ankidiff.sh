#!/usr/bin/bash

[ -z "$1" ] && { echo "You need to specify a file VOCAB or WT"; exit 1; }

[ "$1" = "VOCAB" -o "$1" = "WT" ] || { echo "You need to specify VOCAB or WT"; exit 1; }

for x in dos2unix curl mailx; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

case $1 in
	"VOCAB")
    INPUT="../anki/Jula/Main.txt"
		;;
	"WT")
		INPUT="../anki/Jula-WT/Main.txt" 
		;;
esac

[ -f $INPUT ] || { echo "${INPUT} not found.  Exiting..."; exit 1; }

OUTPUT="${1}.tsv"
MASTER="Main-${1}.tsv"
#make sure we are in master and everthing is committed including files exported from anki

git checkout -b working > /dev/null 2>&1 || { git branch -d working > /dev/null 2>&1 ; git checkout -b working > /dev/null 2>&1 || exit; }

#awk will update existing records but not delete
#awk '{a[$1]=$0}END{for(i in a)print a[i]}' ${MASTER} ${INPUT} | grep -v "Note ID" | sort | dos2unix > ${OUTPUT} 
#mv ${OUTPUT} ${MASTER}

echo here

cat ${INPUT} | awk -F '\t' -v date=$(date '+%Y%m%d%H%M%S') ' 
    BEGIN { OFS="\t" } 
		{ if ( $2=="SYNCDATE" ) { $6=date; $5="anki" }; { print $0 } }' | grep -v "noteid" | sort > ${OUTPUT}

meld ${MASTER} ${OUTPUT} &

git checkout main && git branch -d working
