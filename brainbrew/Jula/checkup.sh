#!/usr/bin/bash
main=./src/data/Main.tsv
update=./src/data/update.tsv
json=./build/Jula/deck.json
echo $(wc -l $main) Total records in Main.tsv
echo $(rg delete $main | wc -l) deletes in Source Main
csvclean -t -v -n $main 
echo $(wc -l $update) Total records in update.tsv
csvclean -t -v -n $update 

jq -r '[.notes[]|{guid:.guid}]' build/Jula/deck.json | yq -y | wc -l

awk 'x[$1]++ == 1 { print $1 " is duplicated"}' $main
