#!/usr/bin/bash
#PURPOSE:  update the main.tsv file with the records from headwords-5.tsv (dictionary)
# fields note1 and note4 , example 1 to 4, text 2 to 4 not overwritten
# this should also add any new items from headword-5.tsv to main.tsv
# after running this script, make edits to import.tsv.
# then move to upate.tsv and run the next script to update main.tsv
# then run brainbrew to update crowdanki file
# then import to anki
set -o errexit
set -o nounset
set -o pipefail
for x in csvtool csvclean gawk dos2unix rg; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

UPDATE=./src/data/update.tsv
HEADWORDS="./src/data/headwords-5.tsv"
MAIN="./src/data/Main.tsv"

function validate {
  csvclean -n -v -t $1
  awk 'BEGIN{FS="\t"} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
}

#csvlint -delimiter '\t' $OUT

validate $UPDATE

#CSVcnt=`awk 'BEGIN{FS="\t"}END{print NF}' $UPDATE`
#if [[ $CSVcnt != 64 ]] ; then
#  echo "ERR0R - FPOS $UPDATE is NOT valid!"
#fi


#awk 'BEGIN{FS="\t"} NF!=64 {print "Error line " NR " " NF " fields";exit 1} END{print  NF " fields"}' $UPDATE

./report.sh
