#!/usr/bin/bash
#The update file from step 1 will contain all the records from main.tsv
# and additional new records and updated records from headwords-5
# these records will have been merged with copy of main.tsv while preserving certain fields:
# example-4
# speech
# english
# others??
#set -o errexit
set -o nounset
set -o pipefail
source ./functions.sh
for x in csvtool csvclean gawk dos2unix rg brainbrew; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 1
  }
done

UPDATE=./src/data/update.tsv

git diff-index --quiet HEAD -- || git commit -a -m "updated on: $(date) by $0"

sed -n '/^\s*$/p' $UPDATE | wc -l
sed -i '/^\s*$/d' $UPDATE
git diff-index --quiet HEAD -- || git commit -a -m "sed del blank lines by $0"
validate $UPDATE

#make sure to pull first to avoid merge conflicts
git -C ./build/Jula pull origin main
#update crowdanki deck.json
#even if records were edited in anki (only the fields above), there may be new records from headwords
#so update crowdanki json file
brainbrew run ./recipes/update.yaml
#git -C ./build/Jula diff-index --quiet HEAD -- || git -C ./build/Jula commit -a -m "brainbrew run update by $0"
#./report.sh

#update Main.tsv
#then bring the changes to main.tsv
brainbrew run ./recipes/anki_to_source.yaml
git diff-index --quiet HEAD -- || git commit -a -m "brainbrew run anki to source by $0"
git push origin main

git -C ./build/Jula diff-index --quiet HEAD -- || git -C ./build/Jula commit -a -m "updated on: $(date) by ./upd-step2.sh"
git -C ./build/Jula push origin main

./report.sh
#delete files in Main.tsv
sed -i /delete/d ./src/data/Main.tsv
git diff-index --quiet HEAD -- || git commit -a -m "sed delete records by $0"
echo "Records deleted in Main.tsv.  Go now and import crowdanki to Anki and delete records there"
