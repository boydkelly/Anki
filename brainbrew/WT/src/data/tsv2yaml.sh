#!/usr/bin/bash
cmd="uuidgen"
#
tsv="./Main.tsv"
out="./out.yaml"
echo "xdxf:" > $out
echo "  lexicon:" >> $out
echo "    ar:" >> $out
awk -v out=$out -v cmd="$cmd" -v uuid="$($cmd)" -F '\t' 'BEGIN { test="out.yaml"; }
                 { uuid4 = ((cmd | getline xout) > 0 ? xout : uuid) }
                 { close(cmd) }
                 { print  "      - k:" >> out}
                 { print  "          - " "\047" "@xml:lang" "\047" ": dyu" >> out}
                 { print  "            " "\047" "@cmt"  "\047" ": " $3 >> out}
                 { printf ("            \047#text\047: \"%s\"\n", $2) >> out}
                 { print  "          - " "\047" "@xml:lang" "\047" ": dyu_CI" >> out}
                 { printf ("            \047#text\047: \"%s\"\n", $20) >> out}
                 { print  "        def: " >> out}
                 { print  "          def: " >> out}
                 { print  "            gr: " >> out}
                 { print  "              abbr: " >> out}
                 { print  "            def: " >> out}
                 { print  "              - " "\047" "@xml:lang" "\047" ": fr" >> out}

                 { if ($64 ~ /dp/) {
                   printf ("                co: \"%s\"\n", $16) >> out;
                 }
                 }

                 { printf ("                deftext: \"%s\"\n", $5) >> out}

                 { if ($27)
                   {
                    print  "                ex:" >> out;
                    print  "                  - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                    " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                    ex_orig: " $27 >> out;
                    print  "                    ex_tran: " $28 >> out;
                 }
                 }
                 { if ($29)
                   {
                    print  "                  - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                    " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                    ex_orig: " $29 >> out;
                    print  "                    ex_tran: " $30 >> out;
                 }
                 }
                 { if ($31)
                   {
                    print  "                  - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                    " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                    ex_orig: " $31 >> out;
                    print  "                    ex_tran: " $32 >> out;
                 }
                 }
                 { if ($33)
                   {
                    print  "                  - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                    " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                    ex_orig: " $33 >> out;
                    print  "                    ex_tran: " $34 >> out;
                 }
                 }
                 { print  "                sr: " >> out}
                 { print  "                  kref: " >> out}

                 { if ($20) {
                   printf ("                  - \047@type\047: syn\n") >> out;
                   printf ("                    \047#text\047: \"%s\"\n", $20) >> out;

                 }
                 }
                 { if ($21) {
                   printf ("                  - \047@type\047: syn\n") >> out;
                   printf ("                    \047#text\047: \"%s\"\n", $21) >> out;

                 }
                 }
                 { if ($22) {
                   printf ("                  - \047@type\047: syn\n") >> out;
                   printf ("                    \047#text\047: \"%s\"\n", $22) >> out;
                 }
                 }
                 { if ($23) {
                   printf ("                  - \047@type\047: syn\n") >> out;
                   printf ("                    \047#text\047: \"%s\"\n", $23) >> out;
                 }
                 }
                 { print  "                categ: " >> out}
                 { if ($64 ~ /dp/)
                   { print "                   - dp" >> out}
                 }
                 { print  "              - " "\047" "@xml:lang" "\047" ": bm" >> out}
                 { print  "                deftext: " $22 >> out}' $tsv


#                   cmd="uuidgen"
#
#awk -v cmd="$cmd" -v uuid="$($cmd)" '
#BEGIN {FS=OFS="\t"}
#$2 ~ /^[[:blank:]]*$/ {
#   $2 = ((cmd | getline out) > 0 ? out : uuid)
#   close(cmd)
#} 1' file | column -t
