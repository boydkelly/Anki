BEGIN { FS=OFS="\t" }
NR==FNR {
    file1rec[$1] = $0
    next
}
$1 in file1rec {
    split(file1rec[$1],file1flds)
    $2 = file1flds[2]
    $4 = file1flds[4]
    $5 = file1flds[5]
    $6 = file1flds[6]
    $7 = file1flds[7]
    $8 = file1flds[8]
    $9 = file1flds[9]
    $10 = file1flds[10]
    $11 = file1flds[11]
    $12 = file1flds[12]
    $13 = file1flds[13]
    $14 = file1flds[14]
    $15 = file1flds[15]
    $17 = file1flds[17]
    $18 = file1flds[18]
    $19 = file1flds[19]
    $27 = file1flds[27]
    $28 = file1flds[28]
    $29 = file1flds[29]
    $30 = file1flds[30]
    $31 = file1flds[31]
    $32 = file1flds[32]
    $33 = file1flds[33]
    $34 = file1flds[34]
    $35 = file1flds[35]
    $36 = file1flds[36]
    $37 = file1flds[37]
    $38 = file1flds[38]
    $39 = file1flds[39]
    $40 = file1flds[40]
    $41 = file1flds[41]
    $42 = file1flds[42]
    $43 = file1flds[43]
    $44 = file1flds[44]
    $45 = file1flds[45]
    $46 = file1flds[46]
    $47 = file1flds[47]
    $48 = file1flds[48]
    $49 = file1flds[49]
    $50 = file1flds[50]
    $51 = file1flds[51]
    $52 = file1flds[52]
    $53 = file1flds[53]
    $54 = file1flds[54]
    $55 = file1flds[55]
    $56 = file1flds[56]
    $57 = file1flds[57]
    $58 = file1flds[58]
    $59 = file1flds[59]
    $60 = file1flds[60]
    $61 = file1flds[61]
    $62 = file1flds[62]
    $64 = file1flds[64]
    delete file1rec[$1]
}
{ print }
END {
    for (key in file1rec) {
        print file1rec[key]
    }
}
