#!/usr/bin/bash

set -o errexit
set -o nounset
set -o pipefail

for x in csvtool csvclean gawk dos2unix rg; do
	type -P $x >/dev/null 2>&1 || {
		echo >&2 "${x} not installed.  Aborting."
		exit 1
	}
done

function validate {
	echo $1
	csvclean -n -v -t $1
	awk 'BEGIN{FS="\t"} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
}

for tsv in Zanga.tsv; do
	validate src/data/$tsv
	[[ -f src/data/$tsv ]] && echo $(wc -l src/data/$tsv) Total records in $tsv
	[[ -f src/data/$tsv ]] && echo $(rg delete src/data/$tsv | wc -l) deletes in $tsv
	[[ -f src/data/$tsv ]] && awk 'x[$1]++ == 1 { print $1,$2,$4" is duplicated"}' src/data/$tsv
done

echo $(rg delete build/Zanga/deck.json | wc -l) deletes in Anki json
echo $(jq -r '[.notes[]|{guid:.guid}]' build/Zanga/deck.json | yq -P -o yaml | wc -l) total records in Anki json
