#!/usr/bin/bash

[ -z "$1" ] && { echo "You need to specify a file VOCAB or WT"; exit 1; }

[ "$1" = "VOCAB" -o "$1" = "WT" ] || { echo "You need to specify VOCAB or WT"; exit 1; }

for x in dos2unix curl mailx; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

case $1 in
	"VOCAB")
    ANKISOURCE="../anki/Jula/Main.txt"
	  SHEETSOURCE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=1005751682&single=true&output=tsv"
		;;
	"WT")
		ANKISOURCE="../anki/Jula-WT/Main.txt" 
		;;
esac

[ -f $ANKISOURCE ] || { echo "${ANKISOURCE} not found.  Exiting..."; exit 1; }

ANKIOUTPUT="${1}-anki.tsv"
SHEETOUTPUT="${1}-sheet.tsv"
MASTER="Main-${1}.tsv"

#make sure we are in master and everthing is committed including files exported from anki

DIRECTORY="/home/bkelly/dev/dl /home/bkelly/drive/personal/language/Jula/Master"

for d in $DIRECTORY; do
  [ -d ${d} ] && cd ${d} || { echo "Directory $d not found."; exit 1; } 

  git checkout main --quiet
  if git diff --exit-code --quiet; then 
	  echo ""
  else
    echo "check git status of $d before proceeding"; exit 1; 
  fi
  git pull origin main --quiet
done

git checkout -b working > /dev/null 2>&1 || { git branch -d working > /dev/null 2>&1 ; git checkout -b working > /dev/null 2>&1 || exit; }

#awk will update existing records but not delete
#awk '{a[$1]=$0}END{for(i in a)print a[i]}' ${MASTER} ${ANKISOURCE} | grep -v "Note ID" | sort | dos2unix > ${ANKIOUTPUT} 
#mv ${ANKIOUTPUT} ${MASTER}


cat ${ANKISOURCE} | awk -F '\t' -v date=$(date '+%Y%m%d%H%M%S') ' 
    BEGIN { OFS="\t" } 
		{ if ( $2=="SYNCDATE" ) { $6=date; $5="anki" }; { print $0 } }' | grep -v "noteid" | sort > ${ANKIOUTPUT}

curl -L "${SHEETSOURCE}" > ${SHEETOUTPUT} || { echo "Download ${SHEETOUTPUT} failed"; exit 1; }
[ -f "${SHEETOUTPUT}" ] || { echo "${SHEETOUTPUT} not found."; exit 1; } 

awk '{a[$1]=$0}END{for(i in a)print a[i]}' ${MASTER} ${SHEETOUTPUT} | grep -v "noteid" | sort | dos2unix > tmp.tsv 


meld tmp.tsv ${MASTER} ${ANKIOUTPUT} & 

