#!/usr/bin/bash

set -o errexit
set -o nounset
set -o pipefail

for x in csvtool csvclean gawk dos2unix rg; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 1
  }
done
source ./functions.sh

for tsv in Main.tsv update.tsv lexicon.tsv; do
  validate src/data/$tsv
  [[ -f src/data/$tsv ]] && echo $(wc -l src/data/$tsv) Total records in $tsv
  [[ -f src/data/$tsv ]] && echo $(rg delete src/data/$tsv | wc -l) deletes in $tsv
  [[ -f src/data/$tsv ]] && awk 'x[$1]++ == 1 { print $1,$2,$4" is duplicated"}' src/data/$tsv
done

echo $(rg delete build/Jula/deck.json | wc -l) deletes in Anki json
echo $(jq -r '[.notes[]|{guid:.guid}]' build/Jula/deck.json | yq -P -o yaml | wc -l) total records in Anki json
