#!/usr/bin/bash

[ -z "$1" ] && { echo "You need to specify a file VOCAB or WT"; exit 1; }

[ "$1" = "VOCAB" -o "$1" = "WT" -o "$1" = "BIBLE" ] || { echo "You need to specify VOCAB or WT"; exit 1; }

for x in dos2unix curl mailx; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

case $1 in
	"VOCAB")
	  GETFILE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=1005751682&single=true&output=tsv"
		;;
	"WT")
		GETFILE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=703917505&single=true&output=tsv"
                ;;
	"BIBLE")
		GETFILE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=742236706&single=true&output=tsv"
		;;
esac

OUTPUT="${1}.tsv"
MAIN="Main-${OUTPUT}"

#make sure we are in main and everthing is committed including files exported from anki

git checkout -b working || exit
curl -L "${GETFILE}" > ${OUTPUT} || { echo "Download ${OUTPUT} failed"; exit 1; }
[ -f "${OUTPUT}" ] || { echo "${OUTPUT} not found."; exit 1; } 

awk '{a[$1]=$0}END{for(i in a)print a[i]}' ${MAIN} ${OUTPUT} | grep -v "noteid" | sort | dos2unix > tmp.tsv

mv tmp.tsv ${MAIN}
rm ${OUTPUT}

git show | mail -s "$0 updates on $(date -Im)" 'bkelly@coastsystems.net'

#cat ${OUTPUT} | awk -F '\t' -v date=$(date '+%Y%m%d%H%M%S') ' 
#    BEGIN { OFS="\t" } 
#		{ if ( $2=="SYNCDATE" ) { $10=date; $9="sheets" }; { print $0 } }' | grep -v "Note ID" | sort | dos2unix > ${MAIN}

#awk -F'\t' '$1' "${OUTPUT}" | grep -v "Note ID" | sort | dos2unix > ${MAIN}

git commit -a -m "updated spreadsheet branch by $0 script"
git checkout main
#git checkout working ${MAIN} 
git merge working
git branch -d working
git push origin main

