#!/usr/bin/bash
FR_OUTPUT="$HOME/dev/web-prod/content/fr/include/w100.tsv"
EN_OUTPUT="$HOME/dev/web-prod/content/en/include/w100.tsv"
MASTER="Master-VOCAB.tsv"
GETFILE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=1005751682&single=true&output=tsv"
#curl -L "${GETFILE}" > tmp.tsv || { echo "Download ${OUTPUT} failed"; exit 1; } 
#awk 'BEGIN {FS="\t"; OFS="\t"} /w100/ {print $5,$2,$6}' tmp.tsv | sort -t$'\t' -k3 -n > ${OUTPUT} 
echo -e "Rang\tJula\tFrançais\tUsage" > ${FR_OUTPUT} 
echo ${FR_OUTPUT}
curl -L "${GETFILE}"  | awk 'BEGIN {FS="\t"; OFS="\t"} /w100/ {print $6,$5,$2,$7}' | sort -t$'\t' -k1 -n >> ${FR_OUTPUT} 
echo -e "Frequency\tJula\tEnglish\tUsage" > ${EN_OUTPUT} 
curl -L "${GETFILE}"  | awk 'BEGIN {FS="\t"; OFS="\t"} /w100/ {print $6,$5,$18,$7}' | sort -t$'\t' -k1 -n >> ${EN_OUTPUT} 
