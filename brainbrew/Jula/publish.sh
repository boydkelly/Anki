#!/usr/bin/bash
file=lexiquejula.adoc
out=vocabulaire.adoc
fr=francais.tsv
sh ./headwords2xml.sh
#mv headwords.xml temp.xml
#xsltproc sort.xsl headwords.xml | xsltproc headwords.xsl - > $out

#xslt3 -xsl:sort.xsl -s:temp.xml -o:temp2.xml
xslt3 -xsl:headwords-alpha.xsl -s:headwords.xml -o:$out
#rm temp*

git commit -a -m $0 && git push --quiet origin master
cp -v $file ~/dev/julakan-docs/docs/modules/lexique-coul/pages/
cp -v $out ~/dev/julakan-docs/docs/modules/lexique-coul/partials/
cp -v $fr ~/dev/julakan-docs/docs/modules/lexique-coul/partials/
#cp $out ~/dev/web-prod/content/dyu/page/
cd /var/home/bkelly/dev/julakan-docs/ && git commit -a -m $0
git checkout web
cd ~/dev/julakan-docs/docs/modules/lexique-coul/partials/
git checkout main $fr 
git checkout main $out
cp -v * ../_include/
git commit -a -m $0
git checkout main
cd /var/home/bkelly/dev/julakan-docs/ 
./webupdate.sh
#&& git push --quiet origin master

#pushd ~/dev/julakan-docs/pages/ && git add $out && git commit $out -m $out && git push --quiet origin master
#popd
#pushd ~/dev/web-prod/content/dyu/page/  && git add $out && git commit $out -m $out && git push --quiet origin master
cd ~/Documents/Jula/lexique-coul
