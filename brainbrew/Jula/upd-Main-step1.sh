#!/usr/bin/bash
#this simply takes the current Main.csv file 
#in sorted order to edit for 
#subsequent reimporting to crowdanki.
set -o errexit
#set -o nounset
set -o pipefail
[ -z "$1" ] && { echo "Specify french or jula"; exit 1; }

case $1 in
	"jula")
    SORT="-k2"
		;;
	"french")
		SORT="-k5" 
		;;
esac

MAIN="/var/home/bkelly/Anki/brainbrew/Jula/src/data/Main.tsv"
UPDATE=./src/data/update.tsv

function validate {
[ -f ${1} ] || { echo "${1} not found.  Exiting..."; exit 1; }
echo Validating $1
  csvclean -n -v -t $1
  awk 'BEGIN{FS="\t"} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
}

validate $MAIN
#sort by... (pick one)
cat $MAIN | (read -r; printf "%s\n" "$REPLY"; sort -t $'\t' $SORT) > $UPDATE
validate $UPDATE
echo "Created new file $UPDATE sorted by $1"
echo $?
