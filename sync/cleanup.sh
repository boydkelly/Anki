#!/usr/bin/bash
[[ "$PWD" == "/var/home/bkelly/Anki/sync" ]] || exit
echo hello
find ../anki/ -type f ! -name Main.txt -delete
find ../anki/ -type d -empty -delete
pushd ../anki
pwd
ln -rs ./WT ./Jula-WT
ln -rs ./Jula-1-Vocabulaire/ ./Jula
popd
