#!/usr/bin/bash

type -P dos2unix >/dev/null 2>&1 || { echo >&2 "dos2unix not installed.  Aborting."; exit 1; }

NEW="/home/bkelly/Downloads/Dico FrancoJula - Export-New.tsv"

[ -f "${NEW}" ] || { echo "${NEW} not found."; exit 1; } 

git checkout master

git pull origin master
git checkout -b spreadsheet
awk -F'\t' '$1' "${NEW}" | grep -v "Note ID" | dos2unix >> Jula-Master-Vocab.tsv
git commit -a -m "updated new by script"
git checkout master
git merge spreadsheet

git branch -d spreadsheet
git push origin master

cp *.tsv ~/dev/blog/static/download/
pushd ~/dev/blog
git commit -a -m "anki updates by script"
git push origin master
popd
git status

#git checkout --patch spreadsheet merge/Jula-Vocab.tsv
#git checkout --patch spreadsheet merge/Jula-Vocab.tsv
