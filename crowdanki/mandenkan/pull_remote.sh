#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=project

#[ -f ${HEADWORDS} ] || { echo "${HEADWORDS} not found.  Exiting..."; exit 1; }

#for x in tmp xml xsd yaml adoc json; do export "${x}=${project}.$x"; done

for x in ls echo ; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

for x in WT Jula Examples ; do
  echo pull $x
  git -C $x pull origin
done

echo "Now go to Anki crowdanki import from disk"
