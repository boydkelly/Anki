#!/usr/bin/bash

[ -z "$1" ] && { echo "You need to specify a file VOCAB or WT"; exit 1; }

[ "$1" = "VOCAB" -o "$1" = "WT" -o "$1" = "BIBLE" ] || { echo "You need to specify VOCAB or WT"; exit 1; }

for x in dos2unix curl mailx; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

case $1 in
 
	"VOCAB")
	  SHEETSOURCE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=1005751682&single=true&output=tsv"
		;;
	"WT")
		SHEETSOURCE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=703917505&single=true&output=tsv"
		;;
	"BIBLE")
		SHEETSOURCE="https://docs.google.com/spreadsheets/d/e/2PACX-1vSH5R8xG1ARb4Swjjl8qYbzRZdBsEcTOFGrEkToW1ln3akG3Gchzfd9r6od6EPGRldu_jcx7YMu7lCq/pub?gid=742236706&single=true&output=tsv"
		;;
esac

SHEETOUTPUT="${1}.tsv"
MASTER="Main-${SHEETOUTPUT}"

#make sure we are in master and everthing is committed including files exported from anki

git checkout -b working > /dev/null 2>&1 || { git branch -d working > /dev/null 2>&1 ; git checkout -b working > /dev/null 2>&1 || exit; }
curl -L "${SHEETSOURCE}" > ${SHEETOUTPUT} || { echo "Download ${SHEETOUTPUT} failed"; exit 1; }
[ -f "${SHEETOUTPUT}" ] || { echo "${SHEETOUTPUT} not found."; exit 1; } 

awk '{a[$1]=$0}END{for(i in a)print a[i]}' ${MASTER} ${SHEETOUTPUT} | grep -v "noteid" | sort | dos2unix > tmp.tsv 


diff ${MASTER} tmp.tsv | mail -s ssdiff bkelly@coastsystems.net
meld ${MASTER} tmp.tsv

git checkout main && git branch -d working
#cat ${SHEETOUTPUT} | awk -F '\t' -v date=$(date '+%Y%m%d%H%M%S') ' 
#    BEGIN { OFS="\t" } 
#		{ if ( $2=="SYNCDATE" ) { $10=date; $9="sheets" }; { print $0 } }' | grep -v "Note ID" | sort | dos2unix > ${MASTER}

#awk -F'\t' '$1' "${SHEETOUTPUT}" | grep -v "Note ID" | sort | dos2unix > ${MASTER}

#git checkout master
#git branch -d working 
